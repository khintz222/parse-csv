import java.util.*

data class Product (
                    var InvoiceNo:Int,
                    var StockCode:String,
                    var Description:String,
                    var Quantity:String,
                    var InvoiceDate:Date,
                    var UnitPrice:Double,
                    var CustomerID:Int,
                    var Country:String)
{

}
