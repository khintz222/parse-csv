import java.io.File
import java.lang.Exception
import java.text.SimpleDateFormat

fun main() {

    val fileName = "C:\\Users\\krzysztofhi\\Downloads\\ecommerce-data\\data.csv"

    var file = File(fileName).readLines()
    var fileArrayList = ArrayList(file)
    fileArrayList.removeAt(0)

    var salesList = arrayListOf<Product>()

    fileArrayList.forEach { line ->
        try {

            val values = line.split(',')

            val product = Product(
                values[0].toInt(),
                values[1],
                values[2],
                values[3],
                SimpleDateFormat("MM/dd/yyyy k:m").parse(values[4]),
                values[5].toDouble(),
                values[6].toInt(),
                values[7]
            )
            salesList.add(product)

        } catch (e: Exception) {


        }
    }

    val filteredList = salesList.filter { item ->
        item.InvoiceDate.after(SimpleDateFormat("MM/dd/yyyy k:m").parse("12/1/2010 11:00")) &&
                item.InvoiceDate.before(SimpleDateFormat("MM/dd/yyyy k:m").parse("12/1/2010 12:00"))
    }

    filteredList.forEach { item ->
        println("InvoiceNo = ${item.InvoiceNo},StockCode = ${item.StockCode},Description = ${item.Description},Quantity = ${item.Quantity},InvoiceDate = ${item.InvoiceDate},UnitPrice = ${item.UnitPrice},CustomerID = ${item.CustomerID},Country = ${item.Country}")
    }
}
